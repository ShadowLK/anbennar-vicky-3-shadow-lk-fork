﻿COUNTRIES = {
	c:A33 = {
		effect_starting_technology_tier_1_tech = yes
		
		activate_law = law_type:law_theocracy
		activate_law = law_type:law_oligarchy	#cardinals
		activate_law = law_type:law_cultural_exclusion
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_national_militia	#army of the faithful
		activate_law = law_type:law_no_home_affairs

		activate_law = law_type:law_interventionism
		activate_law = law_type:law_protectionism
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_no_colonial_affairs
		activate_law = law_type:law_no_police
		activate_law = law_type:law_religious_schools
		activate_law = law_type:law_charitable_health_system

		activate_law = law_type:law_censorship
		activate_law = law_type:law_serfdom_banned
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_slavery_banned

		activate_law = law_type:law_all_races_allowed
		activate_law = law_type:law_dark_arts_banned
		activate_law = law_type:law_amoral_artifice_banned
		activate_law = law_type:law_artifice_only

		set_institution_investment_level = {
			institution = institution_schools
			level = 1
		}
		
		ig:ig_landowners = {
			remove_ideology = ideology_paternalistic
			add_ideology = ideology_rectorate_paternalistic
		}

		ig:ig_intelligentsia = {
			remove_ideology = ideology_anti_clerical
		}
		
		ig:ig_devout = {
			add_ruling_interest_group = yes
			set_interest_group_name = ig_rectorate
			remove_ideology = ideology_moralist
			add_ideology = ideology_ravelian_moralist
			remove_ideology = ideology_pious
			add_ideology = ideology_scholarly
		}
		
		ig:ig_armed_forces = {
			set_interest_group_name = ig_order_of_st_aldresia
			add_ideology = ideology_aldresian
		}
		
		ig:ig_industrialists = {
			remove_ideology = ideology_plutocratic
			add_ideology = ideology_rectorate_plutocratic
		}
	}
}