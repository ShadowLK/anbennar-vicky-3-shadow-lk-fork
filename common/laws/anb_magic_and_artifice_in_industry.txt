﻿law_mundane_production = {
	group = lawgroup_magic_and_artifice
	
	icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
	
	modifier = {
	}
	
	on_enact = {
		recalculate_pop_ig_support = yes
	}
	
	#Will only unban one at a time
	possible_political_movements = {
		law_traditional_magic_only
		law_artifice_only
	}
}

law_traditional_magic_only = {
	group = lawgroup_magic_and_artifice
	
	icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
	
	modifier = {
		country_mages_voting_power_add = 50
	}
	
	on_enact = {
		recalculate_pop_ig_support = yes
	}
	
	possible_political_movements = {
		law_traditional_magic_encouraged
	}
}

law_traditional_magic_encouraged = {
	group = lawgroup_magic_and_artifice
	
	icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
	
	modifier = {
		country_mages_voting_power_add = 20
	}
	
	on_enact = {
		recalculate_pop_ig_support = yes
	}
	
	possible_political_movements = {
		law_traditional_magic_only
		law_artifice_encouraged
	}
}

law_artifice_encouraged = {
	group = lawgroup_magic_and_artifice
	
	icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
	
	on_enact = {
		recalculate_pop_ig_support = yes
	}
	
	modifier = {
	}
	
	possible_political_movements = {
		law_traditional_magic_encouraged
		law_artifice_only
	}
}

law_artifice_only = {
	group = lawgroup_magic_and_artifice
	
	icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
	
	on_enact = {
		recalculate_pop_ig_support = yes
	}
	
	modifier = {
	}
	
	possible_political_movements = {
		law_artifice_encouraged
	}
}