﻿# Anbennar - added our custom laws

# Very high liberalism, e.g. USA

effect_starting_politics_liberal = {
	activate_law = law_type:law_presidential_republic
	activate_law = law_type:law_total_separation
	activate_law = law_type:law_census_voting
	activate_law = law_type:law_per_capita_based_taxation
	activate_law = law_type:law_elected_bureaucrats
	activate_law = law_type:law_private_schools
	activate_law = law_type:law_serfdom_banned
	activate_law = law_type:law_interventionism
	activate_law = law_type:law_mercantilism	
	activate_law = law_type:law_racial_segregation
	activate_law = law_type:law_professional_army
	activate_law = law_type:law_no_migration_controls
	activate_law = law_type:law_professional_army

	#Anbennar
	activate_law = law_type:law_all_races_allowed
	activate_law = law_type:law_dark_arts_banned
	activate_law = law_type:law_amoral_artifice_banned
	activate_law = law_type:law_artifice_encouraged
}

effect_starting_politics_conservative = {
	activate_law = law_type:law_monarchy
	activate_law = law_type:law_state_religion
	activate_law = law_type:law_oligarchy
	activate_law = law_type:law_appointed_bureaucrats
	activate_law = law_type:law_religious_schools
	activate_law = law_type:law_local_police
	activate_law = law_type:law_per_capita_based_taxation
	activate_law = law_type:law_national_supremacy
	activate_law = law_type:law_interventionism
	activate_law = law_type:law_mercantilism
	activate_law = law_type:law_serfdom_banned
	activate_law = law_type:law_censorship
	activate_law = law_type:law_professional_army
	activate_law = law_type:law_migration_controls
	activate_law = law_type:law_professional_army

	#Anbennar
	activate_law = law_type:law_non_monstrous_only	#classic conservative country will be ok with considering non-monstrous. this represent a more conservative but MODERN country
	activate_law = law_type:law_dark_arts_banned
	activate_law = law_type:law_amoral_artifice_banned
	activate_law = law_type:law_artifice_encouraged		#as before, this is not a backwards country but a modern conservative one
}

effect_starting_politics_reactionary = {
	activate_law = law_type:law_monarchy
	activate_law = law_type:law_state_religion
	activate_law = law_type:law_autocracy 
	activate_law = law_type:law_per_capita_based_taxation
	activate_law = law_type:law_local_police
	activate_law = law_type:law_hereditary_bureaucrats
	activate_law = law_type:law_national_supremacy
	activate_law = law_type:law_interventionism
	activate_law = law_type:law_mercantilism
	activate_law = law_type:law_serfdom_banned
	activate_law = law_type:law_censorship
	activate_law = law_type:law_professional_army
	activate_law = law_type:law_migration_controls
	activate_law = law_type:law_professional_army

	#Anbennar
	activate_law = law_type:law_same_race_only
	activate_law = law_type:law_dark_arts_banned
	activate_law = law_type:law_amoral_artifice_banned
	activate_law = law_type:law_traditional_magic_encouraged	#not sure if this should be encouraged or magic only, but I guess a reactionary country will want to go to the past but not too much
}

effect_starting_politics_traditional = {
	activate_law = law_type:law_state_religion
	activate_law = law_type:law_autocracy
	activate_law = law_type:law_land_based_taxation
	activate_law = law_type:law_hereditary_bureaucrats
	activate_law = law_type:law_national_supremacy
	activate_law = law_type:law_traditionalism
	activate_law = law_type:law_right_of_assembly
	activate_law = law_type:law_peasant_levies	
	activate_law = law_type:law_closed_borders
	if = {
		limit = {
			is_country_type = decentralized
		}
		activate_law = law_type:law_chiefdom
		activate_law = law_type:law_isolationism
		activate_law = law_type:law_serfdom_banned
	}
	else = {
		activate_law = law_type:law_monarchy
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_serfdom
	}

	#Anbennar
	activate_law = law_type:law_same_race_only	#equivalent of conservative but leaning towards more traditional fantasy lines of a race-specific empire
	activate_law = law_type:law_dark_arts_banned
	activate_law = law_type:law_amoral_artifice_banned
	activate_law = law_type:law_traditional_magic_only
}
