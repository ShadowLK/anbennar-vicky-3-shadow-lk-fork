﻿gov_absolute_magocracy = {
	transfer_of_power = dictatorial

	male_ruler = "RULER_HIGH_MAGE"
	female_ruler = "RULER_HIGH_MAGE"
	
	possible = {
		has_law = law_type:law_magocracy
		country_has_voting_franchise = no
	}
}

gov_magocratic_oligarchy = {
	transfer_of_power = dictatorial

	male_ruler = "RULER_HIGH_MAGE"
	female_ruler = "RULER_HIGH_MAGE"
	
	possible = {
		has_law = law_type:law_magocracy
		has_law = law_type:law_oligarchy
	}
}

gov_magocratic_democracy = {
	transfer_of_power = dictatorial

	male_ruler = "RULER_HIGH_MAGE"
	female_ruler = "RULER_HIGH_MAGE"
	
	possible = {
		has_law = law_type:law_magocracy
		country_has_voting_franchise = yes
	}
}