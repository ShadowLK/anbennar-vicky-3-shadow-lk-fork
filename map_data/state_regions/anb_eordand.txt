﻿STATE_OPPFROTCOTT = {
    id = 525
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x10C050" "x24D8BD" "x904050" "xA316F5" "xC87A6E" }
    traits = {}
    city = "xc87a6e"
    port = "x904050"
    farm = "xa316f5"
    wood = "x10C050"
    arable_land = 1
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
        bg_whaling = 1
    }
    naval_exit_id = 3115
}
STATE_FOGRACLEAK = {
    id = 605
    subsistence_building = "building_subsistence_farms"
    provinces = { "x4BBE40" "x9C3DDD" "x9CC9DD" "xE762B6" "xED600A" "xFFC0E3" }
    traits = {}
    city = "x9C3DDD"
    port = "x4BBE40"
    farm = "xED600A"
    wood = "xE762B6"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3122
}
STATE_TRIMTHIR = {
    id = 606
    subsistence_building = "building_subsistence_farms"
    provinces = { "x7AEA20" "x9CC997" "x9EF337" }
    traits = {}
    city = "x7AEA20"
    port = "x7AEA20"
    farm = "x9CC997"
    wood = "x9EF337"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3122
}
STATE_MURDKATHER = {
    id = 607
    subsistence_building = "building_subsistence_farms"
    provinces = { "x357C56" "xB0BDDF" "xB510A0" "xC6CBAA" "xCD7775" "xD59DA5" "xD7F1A0" "xE18E42" }
    traits = {}
    city = "xCD7775"
    port = "xD7F1A0"
    farm = "xD59DA5"
    wood = "xB510A0"
    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_silk_plantations }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3122
}
STATE_DEARKTIR = {
    id = 608
    subsistence_building = "building_subsistence_farms"
    provinces = { "x00E145" "x40BD03" "x54ED57" "xD5F2A5" "xF65C3E" "xFF135A" "xFF15A0" }
    traits = {}
    city = "xFF135A"
    port = "xFF15A0"
    farm = "x40BD03"
    wood = "xD5F2A5"
    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3122
}
STATE_PASKALA = {
    id = 609
    subsistence_building = "building_subsistence_farms"
    provinces = { "x190D46" "x370E78" "x519467" "x9B0578" }
    traits = {}
    city = "x9B0578"
    port = "x9B0578"
    farm = "x370E78"
    wood = "x519467"
    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations bg_dye_plantations }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3122
}
STATE_GATHGOB = {
    id = 610
    subsistence_building = "building_subsistence_farms"
    provinces = { "x10869B" "x3020D0" "x370150" "x370A28" "x5E9DB3" "x690B78" "x9B0CA0" }
    traits = {}
    city = "x370A28"
    port = "x370A28"
    farm = "x10869B"
    wood = "x5E9DB3"
    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3122
}
STATE_PELODARD = {
    id = 611
    subsistence_building = "building_subsistence_farms"
    provinces = { "x10F59B" "x4912A9" "xBCA445" "xFF1478" "xFF7800" }
    traits = {}
    city = "x4912A9"
    farm = "x10F59B"
    wood = "xFF7800"
    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations }
    capped_resources = {
        bg_logging = 1
    }
}
STATE_DARHAN = {
    id = 612
    subsistence_building = "building_subsistence_farms"
    provinces = { "x7D04EC" "x8C20F8" "x9140E8" "x9680EC" "xFF1028" "xFF1178" }
    traits = {}
    city = "x8C20F8"
    port = "x9140E8"
    farm = "xFF1178"
    wood = "xFF1028"
    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations bg_silk_plantations }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3123
}
STATE_TRASAND = {
    id = 613
    subsistence_building = "building_subsistence_farms"
    provinces = { "x4B0428" "x9CAC2D" "xE612A0" "xFF0FA0" }
    traits = {}
    city = "xFF0FA0"
    farm = "x4B0428"
    wood = "xE612A0"
    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
}
STATE_EINNSAG = {
    id = 614
    subsistence_building = "building_subsistence_farms"
    provinces = { "x402CA0" }
    traits = {}
    city = "x402CA0"
    port = "x402CA0"
    farm = "x402CA0"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_whaling = 1
    }
    naval_exit_id = 3123
}
STATE_SGLOLAD = {
    id = 615
    subsistence_building = "building_subsistence_farms"
    provinces = { "x285820" "x6E00E8" "x8208F0" "x8E6EC0" }
    traits = {}
    city = "x8E6EC0"
    port = "x6E00E8"
    farm = "x285820"
    wood = "x8208F0"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_dye_plantations }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
        bg_whaling = 1
    }
    naval_exit_id = 3123
}
STATE_IADTH = {
    id = 616
    subsistence_building = "building_subsistence_farms"
    provinces = { "x410728" "x6909A0" "x7200F8" "x9B0878" "xA966AD" "xB84608" }
    traits = {}
    city = "xB84608"
    port = "xA966AD"
    farm = "x9B0878"
    wood = "x6909A0"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3123
}
STATE_RAITHLOS = {
    id = 617
    subsistence_building = "building_subsistence_farms"
    provinces = { "x6903A0" "x6906A0" "x6B00F0" "x7802E8" "x9B0278" "xC67EAD" }
    traits = {}
    city = "x6B00F0"
    port = "x6B00F0"
    farm = "x6906A0"
    wood = "x6903A0"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3123
}
STATE_FOGHARBAC = {
    id = 618
    subsistence_building = "building_subsistence_farms"
    provinces = { "x274C7E" "x66F000" "x6700F4" "x6900E8" "x8710F4" "xC41FF6" "xC887FF" "xD87A4F" }
    traits = {}
    city = "xD87A4F"
    port = "x66F000"
    farm = "x6900E8"
    wood = "x8710F4"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_dye_plantations }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3123
}
STATE_DARTIR = {
    id = 619
    subsistence_building = "building_subsistence_farms"
    provinces = { "x296020" "x587C7E" "x5F00E8" "x6000EC" "x6800F8" "xD76EAD" "xD8724F" }
    traits = {}
    city = "xD76EAD"
    port = "x6800F8"
    farm = "x5F00E8"
    wood = "x6000EC"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3115
}
STATE_GEMRADCURT = {
    id = 620
    subsistence_building = "building_subsistence_farms"
    provinces = { "x587820" "x598020" "x6400E8" "xA746AD" "xC786AD" "xC78A4F" }
    traits = {}
    city = "xC786AD"
    port = "x587820"
    farm = "x598020"
    wood = "xC78A4F"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3115
}
STATE_JHORGASHIRR = {
    id = 621
    subsistence_building = "building_subsistence_farms"
    provinces = { "x5100EC" "x587020" "x58747E" "x6200F4" "x6F00EC" "xD876AD" }
    traits = {}
    city = "x58747E"
    port = "xD876AD"
    farm = "x5100EC"
    wood = "x6F00EC"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3115
}
STATE_GALBHAN = {
    id = 622
    subsistence_building = "building_subsistence_farms"
    provinces = { "x27049B" "x489020" "x4C4C7E" "x4C547E" "x4F00F8" "x5000E8" }
    traits = {}
    city = "x4C4C7E"
    port = "x4F00F8"
    farm = "x5000E8"
    wood = "x489020"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3115
}
STATE_TRIMGARB = {
    id = 623
    subsistence_building = "building_subsistence_farms"
    provinces = { "x55A085" "xB5100C" "xC18317" "xD97EAD" }
    traits = {}
    city = "xC18317"
    farm = "x55A085"
    wood = "xD97EAD"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
    naval_exit_id = 3115
}
STATE_ANHOLTIR = {
    id = 624
    subsistence_building = "building_subsistence_farms"
    provinces = { "x478820" "x478C7E" "x6300F8" }
    traits = {}
    city = "x478820"
    port = "x478820"
    farm = "x478C7E"
    wood = "x6300F8"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3115
}
STATE_ARMONADH = {
    id = 625
    subsistence_building = "building_subsistence_farms"
    provinces = { "x425520" "x468020" "x6500EC" }
    traits = {}
    city = "x6500EC"
    port = "x6500EC"
    farm = "x468020"
    wood = "x425520"
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3115
}
STATE_EGASACH = {
    id = 626
    subsistence_building = "building_subsistence_pastures"
    provinces = { "xCB46AD" "xF00190" }
    traits = {}
    city = "xCB46AD" #Random




    farm = "xCB46AD" #Random




    wood = "xF00190" #Random




    arable_land = 1
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
}
STATE_DOMANDROD = {
    id = 627
    subsistence_building = "building_subsistence_farms"
    provinces = { "x055BC1" "x143BE3" "x1CCF57" "x21BC94" "x3D548C" "x4697B1" "x4B447E" "x51CA5E" "x5B51E3" "x64F79C" "x822A29" "x8508E3" "x971B42" "xA4BEC7" "xA9E347" "xAA91F9" "xABC6D2" "xCFF6C0" "xD7C642" "xDAAE24" "xDC43C8" "xF2C8C5" "xF85904" }
    traits = {}
    city = "xD7C642" #Random




    farm = "xD7C642" #Random




    wood = "xD7C642" #Random




    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
}
