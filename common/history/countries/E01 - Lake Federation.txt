﻿COUNTRIES = {
	c:E01 = {
		effect_starting_technology_tier_4_tech = yes

		add_technology_researched = academia
		add_technology_researched = urban_planning
		add_technology_researched = centralization
		add_technology_researched = law_enforcement
		add_technology_researched = egalitarianism
		
		add_taxed_goods = g:liquor
		add_taxed_goods = g:services
		add_taxed_goods = g:tobacco
		add_taxed_goods = g:porcelain
		add_taxed_goods = g:tea
		add_taxed_goods = g:luxury_clothes
		add_taxed_goods = g:meat
		
		activate_law = law_type:law_parliamentary_republic
		activate_law = law_type:law_universal_suffrage
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_peasant_levies
		activate_law = law_type:law_guaranteed_liberties

		activate_law = law_type:law_interventionism
		activate_law = law_type:law_isolationism
		activate_law = law_type:law_consumption_based_taxation
		activate_law = law_type:law_no_colonial_affairs
		activate_law = law_type:law_no_police
		activate_law = law_type:law_religious_schools
		activate_law = law_type:law_no_health_system

		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_serfdom_banned
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_no_social_security
		activate_law = law_type:law_closed_borders
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_same_race_only
		activate_law = law_type:law_dark_arts_banned
		activate_law = law_type:law_amoral_artifice_banned
		activate_law = law_type:law_mundane_production

	}
}