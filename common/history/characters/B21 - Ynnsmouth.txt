CHARACTERS = {
	c:B21 = {
		create_character = { #Last king who recently relinquished the throne
			first_name = Evn_enn
			last_name = Rafhopa_galawebi
			ruler = yes
			noble = yes
			age = 76
			interest_group = ig_landowners
			ig_leader = yes
			ideology = ideology_moderate
			traits = {
				experienced_diplomat cancer
			}
		}
	}
}
