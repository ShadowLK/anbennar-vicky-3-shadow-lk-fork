﻿#Anbennar - changed some colonial ones to enable it for the setting

#Vanilla
gov_banana_republic = {
	transfer_of_power = dictatorial

	male_ruler = "RULER_TITLE_PRESIDENT"
	female_ruler = "RULER_TITLE_PRESIDENT"
	
	possible = {
		has_law = law_type:law_presidential_republic
		country_has_voting_franchise = no
		any_state = {
			has_building = building_banana_plantation
		}
		any_trade_route = {
			actor_market.owner = root
			exporter = root.market
			goods = g:fruit
		}
	}
}

gov_bhutanese_dual_system_temporal = {
	transfer_of_power = dictatorial

	male_ruler = "RULER_TITLE_DRAGON_REGENT"
	female_ruler = "RULER_TITLE_DRAGON_REGENT"
	
	possible = {
		exists = c:BHU 
		c:BHU = ROOT
		has_law = law_type:law_presidential_republic
		has_law = law_type:law_state_religion
		country_has_voting_franchise = no
	}
}

gov_free_city = {	#TODO - add this anbennar style, and maybe change burgomaster tho
	transfer_of_power = presidential_elective

	male_ruler = "RULER_TITLE_BURGOMASTER"
	female_ruler = "RULER_TITLE_BURGOMASTER"
	
	possible = {
		OR = {
			AND = { exists = c:A00 c:A00 = ROOT }
			# AND = { exists = c:HAM c:HAM = ROOT }
			# AND = { exists = c:LUB c:LUB = ROOT }
			# AND = { exists = c:FRM c:FRM = ROOT }
			# AND = { exists = c:DZG c:DZG = ROOT }
		}
		any_scope_state = {
			count <= 1
		}
		has_law = law_type:law_presidential_republic
		country_has_voting_franchise = yes
	}
}

gov_warlord_state = {
	transfer_of_power = dictatorial

	male_ruler = "RULER_TITLE_GENERAL"
	female_ruler = "RULER_TITLE_GENERAL"
	
	possible = {
		has_law = law_type:law_presidential_republic
		has_law = law_type:law_autocracy 
		has_law = law_type:law_peasant_levies

		OR = { #Maybe theres a better trigger?
			root.capital.region = sr:region_testoria
			# root.capital.region = sr:region_south_china
			# root.capital.region = sr:region_manchuria
			# root.capital.region = sr:region_central_asia
		}
	}
}

gov_fascist_state = {
	transfer_of_power = dictatorial

	male_ruler = "RULER_TITLE_PRESIDENT"
	female_ruler = "RULER_TITLE_PRESIDENT"
	
	possible = {
		OR = {
			has_law = law_type:law_presidential_republic
			has_law = law_type:law_parliamentary_republic
		}
		has_law = law_type:law_autocracy 
		coa_fascist_trigger = yes
	}
}

gov_chartered_company = {
	transfer_of_power = presidential_elective
	new_leader_on_reform_government = no

	male_ruler = "RULER_TITLE_GOVERNOR_GENERAL"
	female_ruler = "RULER_TITLE_GOVERNOR_GENERAL"
	
	#Anbennar - edited this for our companies
	possible = {
		has_law = law_type:law_presidential_republic
		#AND = { exists = c:GBR is_subject_of = c:GBR }
		OR = {
			#AND = { exists = c:HBC c:HBC = ROOT }
			#AND = { exists = c:BIC c:BIC = ROOT }
			AND = { exists = c:B03 c:B03 = ROOT }	#CLSTC
			AND = { exists = c:B04 c:B04 = ROOT }	#DTC
			AND = { exists = c:B05 c:B05 = ROOT }	#VG
		}
		is_subject_type = subject_type_dominion
	}
}

gov_colonial_administration = {
	transfer_of_power = presidential_elective
	new_leader_on_reform_government = no

	male_ruler = "RULER_TITLE_GOVERNOR_GENERAL"
	female_ruler = "RULER_TITLE_GOVERNOR_GENERAL"
	
	#Anbennar - edited this for our companies
	possible = {
		has_law = law_type:law_presidential_republic
		OR = {
			#AND = {
			#	AND = { exists = c:NET is_subject_of = c:NET }
			#	AND = { exists = c:DEI c:DEI = ROOT }
			#	is_subject_type = subject_type_dominion
			#}
			AND = {
				#exists = c:SPA
				#is_subject_of = c:SPA
				#c:SPA = {
				#	has_law = law_type:law_monarchy
				#}
				is_subject_type = subject_type_puppet
				is_country_type = colonial
				top_overlord = { NOT = { is_country_type = colonial } }
				any_primary_culture = {
					#has_discrimination_trait = european_heritage
					has_discrimination_trait = cannorian_heritage
				}
				country_is_in_europe = no
			}
		}
	}
}

gov_crown_colony = {
	transfer_of_power = presidential_elective
	new_leader_on_reform_government = no

	male_ruler = "RULER_TITLE_GOVERNOR"
	female_ruler = "RULER_TITLE_GOVERNOR"
	
	possible = {
		has_law = law_type:law_presidential_republic
		exists = c:GBR
		is_subject_of = c:GBR
		c:GBR = {
			has_law = law_type:law_monarchy
		}
		is_subject_type = subject_type_puppet
		is_country_type = colonial
		top_overlord = { NOT = { is_country_type = colonial } }
		any_primary_culture = {
			has_discrimination_trait = european_heritage
		}
		country_is_in_europe = no
	}
}

gov_military_dictatorship = {
	transfer_of_power = dictatorial

	male_ruler = "RULER_TITLE_GENERAL"
	female_ruler = "RULER_TITLE_GENERAL"
	
	possible = {
		has_law = law_type:law_presidential_republic
		has_law = law_type:law_autocracy 
		OR = {
			has_law = law_type:law_mass_conscription
			has_law = law_type:law_professional_army
		}
	}
}

gov_presidential_dictatorship = {
	transfer_of_power = dictatorial

	male_ruler = "RULER_TITLE_PRESIDENT"
	female_ruler = "RULER_TITLE_PRESIDENT"
	
	possible = {
		has_law = law_type:law_presidential_republic
		has_law = law_type:law_autocracy 
		OR = {
			has_law = law_type:law_peasant_levies
			has_law = law_type:law_national_militia
		}
	}
}

gov_junta = {
	transfer_of_power = dictatorial

	male_ruler = "RULER_TITLE_GENERAL"
	female_ruler = "RULER_TITLE_GENERAL"
	
	possible = {
		has_law = law_type:law_presidential_republic
		country_has_voting_franchise = no
		OR = {
			has_law = law_type:law_mass_conscription
			has_law = law_type:law_professional_army
		}
	}
}

gov_presidential_oligarchy = {
	transfer_of_power = dictatorial

	male_ruler = "RULER_TITLE_PRESIDENT"
	female_ruler = "RULER_TITLE_PRESIDENT"
	
	possible = {
		has_law = law_type:law_presidential_republic
		country_has_voting_franchise = no
	}
}

gov_presidential_democracy = {
	transfer_of_power = presidential_elective
	new_leader_on_reform_government = no

	male_ruler = "RULER_TITLE_PRESIDENT"
	female_ruler = "RULER_TITLE_PRESIDENT"
	
	possible = {
		has_law = law_type:law_presidential_republic
		country_has_voting_franchise = yes
	}
}

