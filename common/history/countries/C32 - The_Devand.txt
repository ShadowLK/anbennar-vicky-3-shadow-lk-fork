﻿COUNTRIES = {
	c:C32 = {
		effect_starting_technology_tier_4_tech = yes
		
		activate_law = law_type:law_monarchy
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_cultural_exclusion
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_appointed_bureaucrats		
		activate_law = law_type:law_national_militia
		activate_law = law_type:law_no_home_affairs
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_interventionism
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_no_colonial_affairs
		activate_law = law_type:law_no_police
		activate_law = law_type:law_no_schools
		activate_law = law_type:law_no_health_system
		activate_law = law_type:law_traditional_magic_encouraged
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_serfdom
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_no_social_security
		activate_law = law_type:law_migration_controls
		activate_law = law_type:law_slave_trade
		
		activate_law = law_type:law_same_race_and_humans

		#ig:ig_devout = {
		#	add_ruling_interest_group = yes
		#}
	}
}